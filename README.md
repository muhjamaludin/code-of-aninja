# Tutorial PHP from [Code of a Ninja](https://codeofaninja.com/2013/06/how-to-run-a-php-script.html) #

### Prerequisite
- php 7.4.5
- apache 2.4.43
- mariadb 10.4.11
- phpmyadmin 5.0.2
- or xampp 7.4.5

### Library
- Bootstrap 5.0.0
- jQuery 3.6.0
- bootbox 5.5.2

### How to run
- activate xampp
- go to htdocs on xampp
- `git clone` <this repo>
- `cd <folder>`
- follow instruction on folder