### How to run
- import sql on data/api_db.sql


### API
- GET
    - all product `http://localhost/code-faninja/simple-rest-api/api/product/read.php`
    - one product `http://localhost/code-faninja/simple-rest-api/api/product/read_one.php?id=`<number>
    - all category for drop-down `http://localhost/code-faninja/simple-rest-api/api/category/read.php`
- POST
    - insert product `http://localhost/code-faninja/simple-rest-api/api/product/create.php`
        - payload = `{"name" : "Amazing Pillow 2.0","price" : "199","description" : "The best pillow for amazing programmers.", "category_id" : 2}`
}
    - update product `http://localhost/code-faninja/simple-rest-api/api/product/update.php`
        - payload = `{"id" : 66,"name" : "Amazing Pillow 4.0","price" : "199","description" : "The best pillow for amazing programmers.","category_id" : 2}`
    - delete product `http://localhost/code-faninja/simple-rest-api/api/product/delete.php`
        - payload `{"id" : 666}`
- Search
    - searching product `http://localhost/code-faninja/simple-rest-api/api/product/search.php?s=`<keyword>
- Pagination
    - read paging `http://localhost/code-faninja/simple-rest-api/api/product/search.php`