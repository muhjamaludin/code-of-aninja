<!-- navbar -->
<div class='navbar navbar-default navbar-static-top' role='navigation'>
    <div class='container-fluid'>
        <div class='navbar-header'>
            <!-- to enable navigation dropdown when viewed in mobile device -->
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- change 'your site' to your site name' -->
            <a class='navbar-brand' href="<?php echo $home_url; ?>">Your Site</a>
        </div>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <!-- link to the "cart" page, highlight if current page is cart.php -->
                <li  <?php echo $page_title=="Index" ? "class='active'" : ""; ?>>
                    <a href="<?php echo $home_url; ?>">Home</a>
                </li>
            </ul>

            <?php
                // login and logout options will be here
                // chechk if users / customer was logged in
                // if user was logged in, show "Edit Profile", "Orders" and "Logout" options
                if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']==true && $_SESSION['access_level']=='Customer') {
            ?>

                <ul class="nav navbar-nav navbar-right">
                    <li <?php echo $page_title=='Edit Profile' ? 'class="active"' : ''; ?>>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <?php echo $_SESSION['firstname']; ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo $home_url; ?>logout.php">Logout</a></li>
                        </ul>
                    </li>
                </ul>

            <?    
                }

                // show login and register options here
                else {
                    ?>

                        <ul class="nav navbar-nav navbar-right">
                            <li <?php echo $page_title=="Login" ? "class='active'": ""; ?>>
                                <a href="<?php echo $home_url; ?>"login>
                                    Log In
                                </a>
                            </li>

                            <li <?php echo $page_title=="Register" ? "class='active'" : ""; ?>>
                                <a href="<?php echo $home_url; ?>register">
                                    Register
                                </a>
                            </li>
                        </ul>

                    <?
                }
            ?>
        </div>
    </div>
</div>