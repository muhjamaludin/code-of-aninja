<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- set page title, for seo purposes too -->
    <title><?php echo isset($page_title) ? strip_tags($page_title) : "Store Front"; ?></title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" media="screen">

    <!-- admin custom CSS -->
    <link rel="stylesheet" href="<?php echo $home_url . "libs/css/customer.css" ?>">
</head>
<body>
    
    <!-- include the navigation var -->
    <?php include_once 'navigation.php'; ?>

    <!-- container -->
    <div class='container'>
    
    <?php
        // if given page title is 'Login', do not display the title
        if ($page_title!="Login") {
    ?>
        <div class="col-md-12">
                <div class="page-header">
                    <h1><?php echo isset($page_title) ? $page_title : "The code of a Ninja"; ?></h1>
                </div>
        </div>
    <?php
        }
    ?>
