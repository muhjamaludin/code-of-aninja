<?php

echo "<ul class='pagination mt0'>";

// first page button will be here
if ($page>1) {
    $prev_page = $page - 1;
    echo "<li class='page-item'>";
        echo "<a class='page-link' href='{$page_url}page={$prev_page}' aria-label='Previous'>";
            echo "<span aria-hidden=true'>&laquo;</span>";
        echo "</a>";
    echo "</li>";
}

// clickable page numbers will be here
// find out total pages
$total_pages = ceil($total_rows / $records_per_page);

// rage of num links to show
$range = 1;

// display links to 'rage of pages' around 'current page'
$initial_num = $page - $range;
$condition_limit_num = ($page + $range) + 1;

for ($x=$initial_num; $x<$condition_limit_num; $x++) {
    // be sure '$x is greather than 0' and 'less than or qequal to the $condition_limit_num
    if (($x>0) && ($x<=$total_pages)) {
        // current page
        if ($x == $page) {
            echo "<li class='active'>";
                echo "<a class='page-link' href='javascript::void();'>{$x}</a>";
            echo "</li>";
        }

        // not current page
        else {
            echo "<li class='page-item'>";
                echo "<a class='page-link' href='{$page_url}page={$x}'>{$x}</a>";
            echo "</li>";
        }
    }
}

// last page button will be here
if ($page<$total_pages) {
    $next_page = $page + 1;

    echo "<li class='page-item'>";
        echo "<a class='page-link' href='{$page_url}page={$next_page}' aria-label='Next'>";
            echo "<span aria-hidden=true>&raquo;</span>";
        echo "</a>";
    echo "</li>";
}

echo "</ul>";

?>