<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PDO - Read Records Record - PHP CRUD Tutorial</title>

    <!-- latest compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    
    <style>
        .m-r-lem{ margin-right: 1em; }
        .m-b-lem{ margin-bottom: 1em; }
        .m-l-lem{ margin-left: 1em; }
        .mt0{margin-top:0;}
        th {text-align: center;}
    </style>

    <script>
        // confirm record deletion
        function delete_user(id) {
            const answer = confirm('Are you sure ?')
            if (answer) {
                // if user clicked ok,
                // pass the id to delete.php and execute the delete query
                window.location = 'delete.php?id=' + id
            }
        }
    </script>
</head>
<body>
    
    <!-- container -->
    <div class="container">
        <div class="page-header">
            <h1>Read Product</h1>
        </div>

        <?php

        // include database connection
        include '../config/database.php';

        // PAGINATION VARIABLES
        // page is the current page, if there's nothing set
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        // set records or rows of data per page
        $records_per_page = 5;

        // calculate for the query LIMIT clause
        $from_record_num = ($records_per_page * $page) - $records_per_page;
        
        // delete message prompt will be here
        $action = isset($_GET['action']) ? $_GET['action'] : "";

        // if it was redirected form delete.php
        if ($action == 'deleted') {
            echo "<div class='alert alert-success'>Record was deleted.</div>";
        }

        // select all data
        $query = "SELECT id, name, description, price FROM products ORDER BY id DESC
                    LIMIT :from_record_num, :records_per_page";
        $stmt = $con->prepare($query);
        $stmt->bindParam(":from_record_num", $from_record_num, PDO::PARAM_INT);
        $stmt->bindParam(":records_per_page", $records_per_page, PDO::PARAM_INT);
        $stmt->execute();

        // this is how to get number of rows returned
        $num = $stmt->rowCount();
        
        // link to create record form
        echo "<a href='create.php' class='btn btn-primary m-b-lem'>Create New Product</a>";

        // check if more than 0 record found
        if ($num>0) {
            echo "<table class='table table-hover table-responsive table-bordered'>";

            // creating our table heading
            echo "<tr>";
                echo "<th>ID</th>";
                echo "<th>Name</th>";
                echo "<th>Description</th>";
                echo "<th>Price</th>";
                echo "<th>Action</th>";
            echo "</tr>";

            // retrive our table contentst
            // fetch() is faster than fetchAll()
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                // extract row
                extract($row);

                // creating new table row per recor
                echo "<tr>";
                    echo "<td>{$id}</td>";
                    echo "<td>{$name}</td>";
                    echo "<td>{$description}</td>";
                    echo '<td>$' . "{$price}</td>";
                    echo "<td class='d-flex justify-content-around'>";
                        // read one record
                        echo "<a href='read_one.php?id={$id}' class='btn btn-info m-r-lem'>Read</a>";

                        // we will use this links on next part of this post
                        echo "<a href='update.php?id={$id}' class='btn btn-primary m-r-lem'>Edit</a>";

                        // we will use this links on next part of this post
                        echo "<a href='#' onclick='delete_user({$id});' class='btn btn-danger'>Delete</a>";
            }

            echo "</table>";

            // pagination
            // count total number of rows
            $query = "SELECT COUNT(*) as total_rows FROM products";
            $stmt = $con->prepare($query);

            // execute query
            $stmt->execute();

            // get total rows
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $total_rows = $row['total_rows'];

            // paginate records
            $page_url = "index.php?";
            include_once "paging.php";
        } else {
            echo "<div class='alert alert-danger'>No records found.</div>";
        }

        ?>

    </div> <!-- end .container -->

    <!-- jQuery (necessary for Bootstrap's Javascript Plugins) -->
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <!-- latest compiled and minified Bootstrap Javascript -->
    <script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>