<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PDO - Update a Record - PHP CRUD Tutorial</title>

    <!-- latest compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
</head>
<body>
    
    <!-- container -->
    <div class="container">
        <div class="page-header">
            <h1>Update Product</h1>
        </div>

        <?php

        // get passed parameter value
        $id = isset($_GET['id']) ? $_GET['id'] : die('ERROR: Record ID not found.');

        // include database connection
        include '../config/database.php';

        // read currenct record's data
        try {
            // prepare select query
            $query = "SELECT id, name, description, price FROM products WHERE id = ? LIMIT 0,1";
            $stmt = $con->prepare($query);

            // this is the firt question mark
            $stmt->bindParam(1, $id);

            $stmt->execute();

            // store retrieved row to a variable
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            // values to fill up our form
            $name = $row['name'];
            $description = $row['description'];
            $price = $row['price'];
        }
        // show error
        catch(PDOException $exception) {
            die('ERROR: ' . $exception->getMessage());
        }

        ?>

        <!-- PHP post to update record will be here -->
        <?php
        // check if form submitted
        if ($_POST) {
            try {
                // write update query
                // in this case, it seemed like we have so many filed
                // it is better to label them and not use question marks
                $query = "UPDATE products SET name=:name, description=:description, price=:price WHERE id=:id";

                // prepare query for execution
                $stmt = $con->prepare($query);

                // posted values
                $name= htmlspecialchars(strip_tags($_POST['name']));
                $description = htmlspecialchars(strip_tags($_POST['description']));
                $price = htmlspecialchars(strip_tags($_POST['price']));

                // bind the parameters
                $stmt->bindParam(':name', $name);
                $stmt->bindParam(':description', $description);
                $stmt->bindParam(':price', $price);
                $stmt->bindParam(':id', $id);

                // execute query
                if ($stmt->execute()) {
                    echo "<div class='alert alert-success'>Record was updated.</div>";
                } else {
                    echo "<div class='alert alert-danger'>Unable to update record. Please try again.</div>";
                }
            } catch (PDOException $exception) {
                die('ERROR: ' . $exception->getMessage());
            }
        }

        ?>

        <!-- we have or html table here where the record will be displayed -->
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'] . "?id={$id}"); ?>" method="POST">
            <table class="table table-hover table-responsive table-bordered">
                <tr>
                    <td>Name</td>
                    <td><input type="text" name="name" value="<?php echo htmlspecialchars($name, ENT_QUOTES); ?>" class="form-control"></td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td><textarea name="description" class="form-control"><?php echo htmlspecialchars($description, ENT_QUOTES); ?></textarea></td>
                </tr>
                <tr>
                    <td>Price</td>
                    <td><input type="text" name="price" class="form-control" value="<?php echo htmlspecialchars($price, ENT_QUOTES); ?>"></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" value="Save Changes" class="btn btn-primary">
                        <a href="index.php" class="btn btn-danger">Back to read products</a>
                    </td>
                </tr>
            </table>
        </form>

    </div> <!-- end .container -->

    <!-- jQuery (necessary for Bootstrap's Javascript Plugins) -->
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <!-- latest compiled and minified Bootstrap Javascript -->
    <script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>