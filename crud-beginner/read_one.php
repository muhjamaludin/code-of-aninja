<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PDO - Read Records Record - PHP CRUD Tutorial</title>

    <!-- latest compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
</head>
<body>
    
    <!-- container -->
    <div class="container">
        <div class="page-header">
            <h1>Read Product</h1>
        </div>

        <?php

        // get passed parameter value
        $id = isset($_GET['id']) ? $_GET['id'] : die('ERROR: Record ID not found.');

        // include database connection
        include '../config/database.php';

        // read currenct record's data
        try {
            // prepare select query
            $query = "SELECT id, name, description, price, image FROM products WHERE id = ? LIMIT 0,1";
            $stmt = $con->prepare($query);

            // this is the firt question mark
            $stmt->bindParam(1, $id);

            $stmt->execute();

            // store retrieved row to a variable
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            // values to fill up our form
            $name = $row['name'];
            $description = $row['description'];
            $price = $row['price'];
            $image = htmlspecialchars($row['image'], ENT_QUOTES);
        }
        // show error
        catch(PDOException $exception) {
            die('ERROR: ' . $exception->getMessage());
        }

        ?>

        <!-- we have or html table here where the record will be displayed -->
        <table class="table table-hover table-responsive table-bordered">
            <tr>
                <td>Name</td>
                <td><?php echo htmlspecialchars($name, ENT_QUOTES); ?></td>
            </tr>
            <tr>
                <td>Description</td>
                <td><?php echo htmlspecialchars($description, ENT_QUOTES); ?></td>
            </tr>
            <tr>
                <td>Price</td>
                <td><?php echo htmlspecialchars($price, ENT_QUOTES); ?></td>
            </tr>
            <tr>
                <td>Image</td>
                <td>
                    <?php echo $image ? "<img class='img-fluid img-thumbnail' src='uploads/{$image}' />" : "No image found."; ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <a href="index.php" class="btn btn-danger">Back to read products</a>
                </td>
            </tr>
        </table>

    </div> <!-- end .container -->

    <!-- jQuery (necessary for Bootstrap's Javascript Plugins) -->
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <!-- latest compiled and minified Bootstrap Javascript -->
    <script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>