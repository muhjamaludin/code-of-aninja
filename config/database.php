<?php

// used to connect to the database
$host = "localhost";
$db_name = "my_first_database";
$username = "root";
$password = "";

try {
    $con = new PDO("mysql:host={$host};dbname={$db_name}", $username, $password);
}

// show error
catch (PDOException $exception) {
    echo "Connection error : " . $exception->getMessage();
}

// Use class Database
class Database {

    // specify your own database credentials
    private $host = 'localhost';
    private $db_name = 'my_first_database';
    private $username = 'root';
    private $password = '';
    public $conn;

    // get the database connection
    public function getConnection() {
        $this->conn = null;

        try {
            $this->conn = new PDO("mysql:host=".$this->host. ";dbname=". $this->db_name,
                $this->username, $this->password);
        } catch (PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }

        return $this->conn;
    }
}