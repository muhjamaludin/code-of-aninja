<?php

// database credentials
$host = "localhost";
$db_name = "my_first_database";
$username = "root";
$password = "";

// connect to database
$con = new PDO("mysql:host={$host};dbname={$db_name}", $username, $password);

// prepare select query
$query = "SELECT id, name, description, price FROM products WHERE id = ? LIMIT 0,1";
$stmt = $con->prepare($query);

// sample product ID
$product_id = 3;

// this is the first question mark in the query
$stmt->bindParam(1, $product_id);

// execute our query
$stmt->execute();

// store retrieved row to the 'row' variable
$row = $stmt->fetch(PDO::FETCH_ASSOC);

// show data to user
echo "<div>Name: " . $row['name'] . "</div>";
echo "<div>Description: " . $row['description'] . "</div>";
echo "<div>Price: $" .$row['price'] . "<div>";