            </div>
            <!-- /container -->

        <!-- jQuery -->
        <script src="../assets/js/jquery-3.6.0.min.js"></script>

        <!-- bootstrap javascript -->
        <script src="../assets/js/bootstrap.min.js"></script>

        <!-- bootbox library -->
        <script src="../assets/js/bootbox.min.js"></script>

        <script>
            // JavaScript for deleting product
            $(document).on('click', '.delete-object', function() {
            let id = $(this).attr('delete-id');
            
            bootbox.confirm({
                closeButton: false,
                message: "<h4>Are you sure ?</h4>",
                buttons: {
                    confirm: {
                        label: '<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-check" viewBox="0 0 16 16"><path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z"/></svg>Yes',
                        className: 'btn-danger'
                    },
                    cancel: {
                        label: '<div><svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-x" viewBox="0 0 16 16"><path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/></svg>No</div>',
                        className: 'btn-primary'
                    }
                },
                callback: function (result) {
                    if (result == true) {
                        $.post('delete_product.php', {
                            object_id: id
                        }, function (data) {
                            location.reload()
                        }).fail(function() {
                            alert('Unable to delete.')
                        })
                    }
                }
            })
            return false
        })
        </script>
        
    </body>
</html>