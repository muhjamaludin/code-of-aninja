<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $page_title; ?></title>

    <!-- latest compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">

    <!-- our custom CSS -->
    <link rel="stylesheet" href="../assets/css/custom.css">
</head>
<body>
    
    <!-- container -->
    <div class="container">
        <?php
        // show page header
        echo "<div class='page-header text-center'>
                <h1>{$page_title}</h1>
              </div>";
        echo "<hr>";
        ?>