<?php
// include database and object files
include_once '../config/database.php';
include_once 'objects/product.php';
include_once 'objects/category.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// pass connection to objects
$product = new Product($db);
$category = new Category($db);

// set page headers
$page_title = "Create Product";
include_once "layout_header.php";

// contents will be here
echo "<div class='right-button-margin d-flex flex-row-reverse'>
        <a href='index.php' class='btn btn-primary'>". '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-list-ul" viewBox="0 0 16 16">
        <path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm-3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
      </svg>' ." Read Products </a>
      </div>";
?>

<!-- PHP post code will be here -->
<?php

// if the form was submited - PHP OOP CRUD Tutorial
if ($_POST) {
    // set product property values
    $product->name = $_POST['name'];
    $product->price = $_POST['price'];
    $product->description = $_POST['description'];
    $product->category_id = $_POST['category_id'];
    $image = !empty($_FILES["image"]["name"]) ? sha1_file($_FILES['image']['tmp_name']) . "-" . basename($_FILES["image"]["name"]) : "";
    $product->image = $image;

    // create the product
    if ($product->create()) {
        echo "<div class='alert alert-success'>Product was created.</div>";
        // try to upload the submitted file
        // uploadPhoto() method will return an error message
        echo $product->uploadPhoto();
    }
    // if unable to create the product, tell the user
    else {
        echo "<div class='alert alert-danger'>Unable to create product.</div>";
    }
}

?>

<!-- 'create product' html form will be here -->
<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST" enctype="multipart/form-data">
    <table class="table table-hover table-responsive table-bordered">
        <tr>
            <td>Name</td>
            <td><input type="text" name="name" class="form-control"></td>
        </tr>
        <tr>
            <td>Price</td>
            <td><input type="text" name="price" class="form-control"></td>
        </tr>
        <tr>
            <td>Description</td>
            <td><textarea name="description" class="form-control"></textarea></td>
        </tr>
        <tr>
            <td>Category</td>
            <td>
                <!-- categories from database will be here -->
                <?php

                // read the product categories from the database
                $stmt = $category->read();

                // put them in a select drop-down
                echo "<select class='form-control' name='category_id'>";
                    echo "<option>Select category...</option>";

                    while ($row_category = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        extract($row_category);
                        echo "<option value='{$id}'>{$name}</option>";
                    }
                echo "</select>";

                ?>
            </td>
        </tr>
        <tr>
            <td>Photo</td>
            <td><input type="file" name="image"></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <button tipe="submit" class="btn btn-primary">Create</button>
            </td>
        </tr>
    </table>
</form>

<?php
// footer
include_once "layout_footer.php";
?>