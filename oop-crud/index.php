<?php
// core.php holds pagination variables
include_once '../config/core.php';

// retrieve records here
// include database and object files
include_once '../config/database.php';
include_once 'objects/product.php';
include_once 'objects/category.php';

// instantiate database and objects;
$database = new Database;
$db = $database->getConnection();

$product = new Product($db);
$category = new Category($db);

// query products
$stmt = $product->readAll($from_record_num, $records_per_page);
$num = $stmt->rowCount();

//set page header
$page_title = "Read Products";
include_once  "layout_header.php";

// // contents will be here
// echo "<div class='right-button-margin  d-flex flex-row-reverse'>
//         <a href='create_product.php' class='btn btn-success'> Create Product </a>
//     </div>";

// // display the products if there a any
// if ($num>0) {
//     echo "<table class='table table-hover table-responsive table-bordered'>";
//         echo "<tr>";
//             echo "<th>Product</th>";
//             echo "<th>Price</th>";
//             echo "<th>Description</th>";
//             echo "<th>Category</th>";
//             echo "<th>Actions</th>";
//         echo "</tr>";

//         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
//             extract($row);

//             echo "<tr>";
//                 echo "<td>{$name}</td>";
//                 echo "<td>{$price}</td>";
//                 echo "<td>{$description}</td>";
//                 echo "<td>";
//                     $category->id = $category_id;
//                     $category->readName();
//                     echo $category->name;
//                 echo "</td>";
//                 echo "<td class='d-flex justify-content-around'>";
//                     // read one, edit, and delete button will be here
//                     echo "<a href='read_one.php?id={$id}' class='btn btn-primary left-margin'>
//                             ". '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-list-ul" viewBox="0 0 16 16">
//                             <path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm-3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
//                           </svg>' ." Read
//                         </a>
//                         <a href='update_product.php?id={$id}' class='btn btn-info left-margin'>
//                            ". '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pen-fill" viewBox="0 0 16 16">
//                            <path d="M13.498.795l.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001z"/>
//                          </svg>' . " Edit
//                         </a>
//                         <a delete-id='{$id}' class='btn btn-danger delete-object'>
//                             ". '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-archive-fill" viewBox="0 0 16 16">
//                             <path d="M12.643 15C13.979 15 15 13.845 15 12.5V5H1v7.5C1 13.845 2.021 15 3.357 15h9.286zM5.5 7h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1 0-1zM.8 1a.8.8 0 0 0-.8.8V3a.8.8 0 0 0 .8.8h14.4A.8.8 0 0 0 16 3V1.8a.8.8 0 0 0-.8-.8H.8z"/>
//                           </svg>' ." Delete
//                         </a>";
                   
//                 echo "</td>";
//             echo "</tr>";
//         }

//     echo "</table>";

//     // the page where this paging is used
//     $page_url = "index.php?";

//     // count all products in the database to calculate total pages
//     $total_rows = $product->countAll();

//     // paging buttons here
//     include_once 'paging.php';
// } 
// // tell the user there are no products
// else {
//     echo "<div class='alert alert-info'>No products found.</div>";
// }

// query products
$stmt = $product->readAll($from_record_num, $records_per_page);

// specify the page where paging is used
$page_url = "index.php?";

// count total rows = used for pagination
$total_rows = $product->countAll();

// read_template.php controls how the product list will be rendered
include_once "read_template.php";

// set page footer
include_once "layout_footer.php";
?>