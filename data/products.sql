-- CREATE DATABASE my_first_database;

CREATE TABLE IF NOT EXISTS products (
    id INT AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL,
    description TEXT NOT NULL,
    price DECIMAL(15,2) NOT NULL,
    created DATETIME DEFAULT CURRENT_TIMESTAMP,
    modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) DEFAULT CHARSET=latin1 AUTO_INCREMENT=9;

INSERT INTO products (name, price, description) VALUES 
	('Gatorade', 1.99, 'This is a very good drink for athletes.'),
    ('Eye Glasses', 6, 'It will make you better.'),
    ('Trash Can', 3.95, 'It will help you maintain cleanliness.'),
    ('Mouse', 11.39, 'Very useful if you love your computer.'),
    ('Earphone', 7, 'You need this one if you love music'),
    ('Pillow', 8.99, 'Sleeping well is important');

ALTER TABLE products ADD COLUMN image TEXT AFTER price;
